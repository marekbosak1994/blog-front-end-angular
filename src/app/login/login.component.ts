import {Component, OnInit} from '@angular/core';
import {User} from './user';
import {AuthService} from '../auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.login();
  }

}
