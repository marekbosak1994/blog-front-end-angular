export enum PostStatus {
  DRAFT,
  PUBLISHED,
  DELETED
}
