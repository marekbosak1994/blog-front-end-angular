import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {CallbackComponent} from "./callback/callback.component";
import {AuthGuard} from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home/posts',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: 'app/home/home.module#HomeModule'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'callback',
    component: CallbackComponent
  },
  {
    path: 'admin',
    canLoad: [AuthGuard],
    loadChildren: 'app/admin/admin.module#AdminModule'
  }
];

/*
 {path: '', redirectTo: '/home/booking', pathMatch: 'full'},
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: 'booking', pathMatch: 'full'},
      {
        path: 'booking',
        component: BookingComponent
      },
      {
        path: 'mybookings',
        component: MyBookingsComponent
      },
      {
        path: 'manage',
        component: ManageDashboardComponent,
        children: [
          {path: '', redirectTo: 'cars', pathMatch: 'full'},
          {
            path: 'cars',
            component: PoolCarListComponent
          },
          {
            path: 'cars/edit/:id',
            component: PoolCarDetailComponent
          },
          {
            path: 'cars/add',
            component: PoolCarDetailComponent
          },
          {
            path: 'cars/models',
            component: CarModelDetailComponent
          },
          {
            path: 'users',
            component: UserListComponent
          },
          {
            path: 'users/edit/:id',
            component: UserDetailComponent
          },
          {
            path: 'users/add',
            component: UserDetailComponent
          }
        ]
      }
    ]
  },
  {path: 'denied', component: AccessDeniedComponent},
  {path: '**', component: PageNotFoundComponent}
*/
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
