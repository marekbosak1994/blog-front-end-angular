import {Component, OnInit} from '@angular/core';
import {CategoryManageService} from '../services/category-manage.service';
import {Category} from '../../category';

@Component({
  selector: 'app-category-manage',
  templateUrl: './category-manage.component.html',
  styleUrls: ['./category-manage.component.css'],
  providers: [CategoryManageService]
})
export class CategoryManageComponent implements OnInit {
  categories: Category[];

  constructor(private categoryService: CategoryManageService) {
  }

  ngOnInit() {
    this.categoryService.getAllCategories()
      .subscribe(
        data => this.categories = data,
        error => console.log(error)
      );
  }

}
