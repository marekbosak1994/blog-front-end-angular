import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Post} from '../../post';
import {PostManageService} from '../services/post-manage.service';
import {MatRadioChange, MatRadioGroup, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {PostStatus} from '../../postStatus';
import {fadeIn} from '../../animations';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  providers: [PostManageService],
  animations: [fadeIn]
})
export class PostListComponent implements OnInit {

  myState = PostStatus.DRAFT;
  posts: Post[];
  checked = 'ALL';


  constructor(private postManageService: PostManageService,
              public snackBar: MatSnackBar,
              private router: Router) {

  }

  ngOnInit() {
    this.onLoadPostsBasedOnStatus(this.checked);
  }

  onLoadPostsBasedOnStatus(status: string) {
    this.postManageService.findByStatus(status).subscribe(
      posts => this.posts = posts,
      error => this.handleError(error)
    );
  }

  onDelete(deleted: boolean) {
    this.onLoadPostsBasedOnStatus(this.checked);
    this.openSnackBar();
  }



  openSnackBar() {
    this.snackBar.open('Post moved to trash', 'Undo', {duration: 3000});
  }

  load(status: string) {

  }

  private handleError(error: any) {
    switch (error.exception) {
      case 'io.jsonwebtoken.ExpiredJwtException':
        console.log('Your token expired. Please login again.');
    }
  }

  onChange($event) {
    console.log($event.value);
    this.checked = $event.value;
    this.onLoadPostsBasedOnStatus($event.value);
  }
}
