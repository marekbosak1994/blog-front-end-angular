import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin.component';
import {AuthGuard} from '../auth/auth.guard';
import {AdminDashboardComponent} from './dashboard/dashboard.component';
import {PostListComponent} from './post-list/post-list.component';
import {PostManageComponent} from './post-manage/post-manage.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserManageComponent} from './user-manage/user-manage.component';
import {CategoryManageComponent} from './category-manage/category-manage.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        component: AdminDashboardComponent
      },
      {
        path: 'manage',
        children: [
          {
            path: 'posts',
            component: PostListComponent
          },
          {
            path: 'post/:id',
            component: PostManageComponent
          },
          {
            path: 'post',
            component: PostManageComponent
          },
          {
            path: 'authors',
            component: UserListComponent
          },
          {
            path: 'author/:id',
            component: UserManageComponent
          },
          {
            path: 'author',
            component: UserManageComponent
          },
          {
            path: 'categories',
            component: CategoryManageComponent
          },
          {
            path: '',
            redirectTo: 'posts',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'manage',
        pathMatch: 'full'
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
