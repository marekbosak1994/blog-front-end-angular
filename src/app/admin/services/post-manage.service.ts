import {Injectable} from '@angular/core';
import {Post} from '../../post';
import { Headers, Response, URLSearchParams} from '@angular/http';

import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class PostManageService {

  private url = environment.apiUrl + '/admin';
  private postsUrl = this.url + '/posts';
  private postUrl = this.url + '/posts/';


  constructor(private http: HttpClient) {
  }

  save(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl, JSON.stringify(post), {headers: this.getHeader()});
  }

  getAllPosts(): Observable<Post[]> {
    return this.http.get(this.postsUrl, {headers: this.getHeader()});
  }

  getById(id: string): Observable<Post> {
    return this.http.get<Post>(this.postUrl.concat(id), {headers: this.getHeader()});
  }

  deletePost(id: string) {
    return this.http.delete(this.postUrl.concat(id), {headers: this.getHeader()});
  }

  updatePost(post: Post): Observable<Post> {
    console.log(post);
    return this.http.put<Post>(this.postsUrl, JSON.stringify(post), {headers: this.getHeader()});
  }

  findByStatus(status: string): Observable<Post[]> {
    let params = new HttpParams();
    params = params.append('status', status);
    return this.http.get<Post[]>(this.postsUrl, {
      headers: this.getHeader(),
      params: params
    });
  }

  getHeader(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'));

    return headers;
  }
}
