
import {Injectable} from '@angular/core';
import {Headers, Response} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Image} from '../../image';

@Injectable()
export class UploadImageService {
  private url = environment.apiUrl + '';
  private imagePostUrl = this.url + '/images';

  constructor(private http: HttpClient) {

  }

  uploadImage(image) {
    return this.http.post<Image>(this.imagePostUrl, image, {headers: this.getHeader()});
  }

  getHeader(): HttpHeaders {
    let header = new HttpHeaders();
    header = header.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
    return header;
  }
}
