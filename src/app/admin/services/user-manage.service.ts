import {Injectable} from '@angular/core';
import {Headers, Response, URLSearchParams} from '@angular/http';
import {Author} from '../../author';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserManageService {

  private url = environment.apiUrl + '/admin';
  private authorsUrl = this.url + '/authors';
  private authorUrl = this.url + '/author/';
  private authorUrl2 = this.url + '/author';

  constructor(private http: HttpClient) {
  }


  updateAuthor(author: Author) {
    return this.http.put(this.authorUrl2, JSON.stringify(author), {headers: this.getHeader()}).map((response: Response) => response);
  }

  getHeader(): HttpHeaders {
    let header = new HttpHeaders();
    header = header.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
    header = header.set('Content-type', 'application/json;charset=UTF-8');
    return header;
  }
}
