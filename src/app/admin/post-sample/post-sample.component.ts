import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {PostManageService} from '../services/post-manage.service';
import {Post} from '../../post';
import {PostStatus} from '../../postStatus';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-post-sample',
  templateUrl: './post-sample.component.html',
  styleUrls: ['./post-sample.component.css'],
  providers: [PostManageService]
})
export class PostSampleComponent implements OnInit {


  @Output() onRefresh = new EventEmitter<boolean>();
  @Input() post: Post;

  stats = [
    {value: 'DRAFT', viewValue: 'Draft'},
    {value: 'PUBLISHED', viewValue: 'Publish'}
  ];

  constructor(private postManageService: PostManageService,
              public snackBar: MatSnackBar,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  onChange(selectedStatus: string) {
    this.onUpdatePostStatus(this.post, selectedStatus);
  }

  private onDelete(id: string) {
    this.postManageService.deletePost(id).subscribe(
      data => this.onRefresh.emit(true),
      error2 => console.log(error2.json())
    );
  }

  private onUpdatePostStatus(post: Post, status: string) {
    post.status = status;
    this.postManageService.updatePost(post).subscribe(
      data => {
        this.onRefresh.emit(true);
      },
      error => console.log(error.json())
    );
  }
}
