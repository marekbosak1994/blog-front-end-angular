import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  checked = 'center';

  constructor() {
  }

  ngOnInit() {
  }

  onChange($event) {
    console.log($event);
  }

}
