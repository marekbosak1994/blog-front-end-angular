import {Component, OnInit} from '@angular/core';
import {Author} from '../../author';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserManageService} from '../services/user-manage.service';
import {AuthService} from '../../auth/auth.service';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.css'],
  providers: [UserManageService]
})
export class UserManageComponent implements OnInit {

  rForm: FormGroup;
  id: string;
  author: Author;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private userManageService: UserManageService,
              private router: Router,
              private route: ActivatedRoute) {

    this.rForm = this.fb.group({
      id: [null],
      username: [null, Validators.compose([Validators.required])],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      nickname: [null, Validators.compose([Validators.required])],
      password: [null]
    });
  }

  ngOnInit() {
  }

  initForm(author: Author) {
    console.log(author);
    this.rForm.patchValue({
      id: author.id,
      username: author.username,
      email: author.email,
      nickname: author.nickname
    });
  }

  addOrUpdateAuthor(author: Author) {
    this.userManageService.updateAuthor(author).subscribe(data => console.log(data.json()),
      error => console.log(error));
  }

}
