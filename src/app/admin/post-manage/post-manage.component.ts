import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {PostManageService} from '../services/post-manage.service';
import {Post} from '../../post';
import {Category} from '../../category';
import {Author} from '../../author';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import {AuthService} from '../../auth/auth.service';

import {UploadImageService} from '../services/upload-image.service';
import {CategoryManageService} from '../services/category-manage.service';
import {environment} from '../../../environments/environment';


@Component({
  selector: 'app-post-create',
  templateUrl: './post-manage.component.html',
  styleUrls: ['./post-manage.component.css'],
  providers: [PostManageService, UploadImageService, CategoryManageService]
})
export class PostManageComponent implements OnInit {

  @ViewChild('fileInput') fileInput;
  rForm: FormGroup;
  id: string;
  post: Post;
  author: Author;
  restOfCategories: Category[];
  allCategories: Category[];
  coverImgUrl: string;
  public editorOptions: Object = {
    placeholderText: 'Edit Your Content Here!',
    imageUploadURL: environment.apiUrl + '/images'
    /*imageUploadParams: {
      Authorization: localStorage.getItem('currentUser')
    }*/
  };

  constructor(private fb: FormBuilder,
              private postManageService: PostManageService,
              private categoryService: CategoryManageService,
              private uploadService: UploadImageService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) {

    this.crateForm();
  }

  ngOnInit() {
    this.categoryService.getAllCategories().subscribe(data => this.allCategories = data);
    this.route.paramMap.subscribe(params => this.dealWithParam(params.get('id')));

    this.initForm();
  }

  private crateForm() {
    this.rForm = this.fb.group({
      id: '',
      title: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(50)])],
      description: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(500)])],
      content: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(70000)])],
      categories: [[], [Validators.required]],
      urlName: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(100)])],
      coverImgUrl: ['']
    });
  }

  private initForm() {
    this.rForm
      .controls
      .categories
      .valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe(data => {
        console.log('CATEGORY CHANGED', data);
      });

    /*this.rForm
      .controls
      .content
      .valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe(data => {
        console.log('native fromControl value changes with debounce', data);
      });*/

  }


  updateForm(post: Post) {
    this.post = post;
    console.log(this.post);
    this.rForm.patchValue({
      id: this.post.id,
      title: this.post.title,
      description: this.post.description,
      categories: this.post.categories,
      content: this.post.content,
      urlName: this.post.urlName,
      coverImgUrl: this.post.coverImgUrl
    });
  }

  uploadImage(fileBrowser) {
    if (fileBrowser.files && fileBrowser.files[0]) {
      const formData = new FormData();
      formData.append('file', fileBrowser.files[0]);
      console.log(fileBrowser.files[0]);
      this.uploadService.uploadImage(formData).subscribe(
        data => {
          console.log(data.link);
          this.coverImgUrl = data.link;
        },
        error => console.log(error.json())
      );
    }
  }

  dealWithParam(id: string) {
    if (id !== null) {
      this.postManageService.getById(id).subscribe(data => this.updateForm(data));
    }
  }

  addOrUpdatePost(post: Post) {
    if (post.id) {
      this.updatePost(post);
    } else {
      this.addPost(post);
    }
  }

  private addPost(post: Post) {
    console.log(post);
    this.post = post;
    const auth: Author = {id: 1, username: '', email: '', nickname: ''};
    this.post.author = auth;
    this.post.status = 'DRAFT';
    this.post.coverImgUrl = this.coverImgUrl;
    this.postManageService.save(this.post).subscribe(
      data => this.router.navigate(['/home/post/', data.id]), // this.post = data,
      error => console.log(error.json())
    );
  }

  private updatePost(post: Post) {
    post.author = this.post.author;
    post.categories = this.post.categories;

    this.postManageService.updatePost(post).subscribe(
      data => this.router.navigate(['/admin/manage/posts']),
      error => console.log(error.json())
    );
  }

  onCheckboxChange($event, category: Category) {
    const index = this.getIndexOfCategoryInArray(category);
    console.log('cateogry id: ' + category.id + ' , index: ' + index);

    if ($event.checked) {
      if (index < 0) {
        this.rForm.value.categories.push(category);
      }
    } else {
      if (index >= 0) {
        this.rForm.value.categories.splice(index, 1);
      }
    }

    console.log(this.rForm.value.categories);
  }

  isChecked(category: Category): boolean {
    // console.log(category);
    if (this.rForm.value.categories == null) {
      return false;
    }

    return this.getIndexOfCategoryInArray(category) >= 0;
  }

  getIndexOfCategoryInArray(category: Category): number {
    let indexOfCategory = -1;
    this.rForm.value.categories.some(function (value, index) {
      if (value.id === category.id) {
        indexOfCategory = index;
      }
    });
    return indexOfCategory;
  }
}
