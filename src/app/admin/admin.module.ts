import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin.component';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminDashboardComponent} from './dashboard/dashboard.component';
import {PostListComponent} from './post-list/post-list.component';
import {PostManageComponent} from './post-manage/post-manage.component';
import {AppMaterialModule} from '../app-material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PostSampleComponent} from './post-sample/post-sample.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import {CategoryManageComponent} from './category-manage/category-manage.component';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    AdminComponent,
    PostListComponent,
    PostManageComponent,
    AdminDashboardComponent,
    PostSampleComponent,
    CategoryManageComponent,
    PostSampleComponent,
    UserListComponent,
    UserManageComponent
  ]
})
export class AdminModule {
}
