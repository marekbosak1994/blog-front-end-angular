import {Author} from './author';
import {Category} from './category';

export interface Post {
  id: number;
  author: Author;
  title: string;
  content: string;
  urlName: string;
  coverImgUrl: string;
  description: string;
  created: string;
  lastUpdate: string;
  status: string;
  categories: Category[];
}
