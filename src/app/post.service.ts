import {Injectable} from '@angular/core';
import { Response} from '@angular/http';
import {Post} from './post';

import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PostService {

  private postsUrl = environment.apiUrl + '/posts';
  private postUrl = environment.apiUrl + '/posts/';


  constructor(private http: HttpClient) {
  }

  getPublishedPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl );
  }

  getById(id: string): Observable<Post> {
    console.log(id);
    this.postUrl = this.postUrl.concat(id);
    console.log(this.postUrl);
    return this.http.get<Post>(this.postUrl);
  }

}
