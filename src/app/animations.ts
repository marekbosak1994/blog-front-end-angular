import {trigger, state, animate, transition, style} from '@angular/animations';

export const fadeIn =
  // trigger name for attaching this animation to an element using the [@triggerName] syntax
  trigger('fadeIn', [

    state('void', style({opacity: 0})),

    // route 'enter' transition
    transition(':enter', [

      // animation and styles at end of transition
      animate('.3s', style({opacity: 1}))
    ]),
  ]);
