import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {PostsComponent} from './posts/posts.component';
import {PostDetailComponent} from './post-detail/post-detail.component';

const guestRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'posts', component: PostsComponent
      },
      {
        path: 'post/:id', component: PostDetailComponent
      },
      {
        path: '',
        redirectTo: 'posts',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(guestRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {
}


