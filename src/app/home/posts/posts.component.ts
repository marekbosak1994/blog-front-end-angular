import {Component, OnInit} from '@angular/core';
import {PostService} from '../../post.service';
import {Post} from '../../post';
import {User} from '../../login/user';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [PostService]
})

export class PostsComponent implements OnInit {

  posts: Post[];
  private errorMessage: any;


  constructor(private postService: PostService, private router: Router) {
  }

  ngOnInit(): void {
    console.log('PostComponent ngOnInit start');
    this.postService.getPublishedPosts().subscribe(
      posts => this.posts = posts,
      error => console.error('An error occured', error)
    );
    console.log('PostComponent ngOnInit end');
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occured', error);
    return Observable.throw(error.message || error);
  }

  gotoDetail(id: number): void {
    console.log(id);
    this.router.navigate(['/post', id]);
  }
}
