import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppMaterialModule} from '../app-material.module';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {PostsComponent} from './posts/posts.component';
import {PostDetailComponent} from './post-detail/post-detail.component';

@NgModule({
  imports: [
    CommonModule,
    AppMaterialModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    PostsComponent,
    PostDetailComponent
  ]
})
export class HomeModule {
}
