import 'rxjs/add/operator/switchMap';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {PostService} from '../../post.service';
import {Post} from '../../post';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService]
})
export class PostDetailComponent implements OnInit {

  post: Post;
  trustedContent: SafeHtml;
  id: string;

  constructor(private route: ActivatedRoute,
              private postService: PostService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => this.fetchPostById(params.get('id')));
  }

  private fetchPostById(id: string) {
    if (id !== null) {
      this.postService.getById(id).subscribe(data => this.initPost(data));
    }
  }

  private initPost(data: Post) {
    this.post = data;
    this.trustedContent = this.sanitizer.bypassSecurityTrustHtml(this.post.content);
  }
}
