import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './auth.service';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isLoggedIn = this.authService.isAuthenticated();
    console.log('canactive');
    if (isLoggedIn) {
      return true;
    }

    // this.router.navigate(['/login']);
    return false;

    /*
    if (this.authService.isLoggedIn()) {
      if (this.authService.isLoggedUserAdmin()) {
        return true;
      }

      if (state.url.includes('manage')) {
        this.router.navigate(['/denied']);
        return false;
      }

      return true;
    }

    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
    */
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    const isLoggedIn = this.authService.isAuthenticated();

    if (isLoggedIn) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
