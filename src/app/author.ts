export interface Author {
  id: number;
  username: string;
  nickname: string;
  email: string;
}
